module Day1 where

import Prelude
import Data.List
import Debug.Trace
import qualified Data.Map as Map

adjustWithDefault :: (Ord k, Show k) => (Int -> Int) -> k -> Map.Map k Int -> Map.Map k Int
adjustWithDefault f k m = Map.alter adjustedOrDefault k m
  where adjustedOrDefault Nothing = Just(1)
        adjustedOrDefault o @ _ = fmap f o

findDuplicateValue :: Ord k => Map.Map k Int -> Maybe (k, Int)
findDuplicateValue m = Map.lookupMax $ Map.filter (==2) m

main :: IO ()
main = do input <- readFile "src/main/resources/Day1.txt"
          let inputLines = lines input
          let first = head inputLines
          let ints = map (\l -> read (if (head l) == '+' then (drop 1 l) else l) :: Int) inputLines
          let infValues = (scanl (+) (0) (cycle ints))
          putStrLn ("Part1: " ++ show (sum ints))
          putStrLn ("Part2:" ++ show (find (/= Nothing) $ map findDuplicateValue $ scanl (\m e -> adjustWithDefault (+1) e m) Map.empty infValues))

