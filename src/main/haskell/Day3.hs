{-# LANGUAGE QuasiQuotes #-}

module Day3 where

import Prelude
import Text.Regex.PCRE.Heavy
import Data.List
import Debug.Trace
import qualified Data.Map as Map

data Claim = Claim { claimId:: String
                   , x :: Int
                   , y :: Int
                   , width :: Int
                   , height :: Int
                   } deriving (Show)

toClaim :: (String, [String]) -> Claim
toClaim (_, [id, x, y, w, h]) = Claim id (read x :: Int) (read y :: Int) (read w :: Int) (read h :: Int)

parse :: String -> [Claim]
parse s = map toClaim (scan [re|#(\d*) @ (\d*),(\d*): (\d*)x(\d*)|] s)

coordinates :: Claim -> [(String, Int, Int)]
coordinates Claim{claimId = claimId, x = x, y = y, width = w, height = h} = [(claimId, x + i, y + j) | i <- [0..w - 1], j <- [0..h - 1]]

part1 coordinatesCounts = Map.size $ Map.filter (\l -> length l > 1) coordinatesCounts

part2 allCoordinates coordinatesCounts = head $ allIds \\ coverTwo
  where allIds = nub $ map (\(claimId, _, _) -> claimId) allCoordinates
        coverTwo = nub $ concatMap (\(_, ids) -> ids) (Map.toList (Map.filter (\l -> length l > 1) coordinatesCounts))

main :: IO ()
main = do input <- readFile "src/main/resources/Day3.txt"
          let inputLines = lines input
          let claims = concatMap parse inputLines
          let allCoordinates = concatMap coordinates claims
          let coordinatesCounts = Map.fromListWith (++) $ map (\(claimId, x, y) -> ((x, y), [claimId])) allCoordinates
          putStrLn ("Part1: " ++ show (part1 coordinatesCounts))
          putStrLn ("Part2: " ++ (part2 allCoordinates coordinatesCounts))


