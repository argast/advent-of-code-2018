module Day2 where

import Prelude
import Data.List
import Debug.Trace
import qualified Data.Map as Map


countChars :: String -> Map.Map Char Int
countChars s = Map.fromListWith (+) $ map (\c -> (c, 1)) s

filterRelevant :: Map.Map Char Int -> (Map.Map Char Int, Map.Map Char Int)
filterRelevant m = Map.partition (==2) (Map.filter (\v -> v == 2 || v == 3) m)

accumulate (t2, t3) (m2, m3) = (t2 + nt2, t3 + nt3)
  where nt2 = signum (Map.size m2)
        nt3 = signum (Map.size m3)

boxIntersect :: String -> String -> String
boxIntersect s1 s2 = foldl acc "" (s1 `zip` s2)
  where acc r (c1, c2) = if c1 == c2 then r ++ [c1] else r

part1 inputLines = (fst result) * (snd result)
  where result = foldl accumulate (0, 0) maps
        maps = map (filterRelevant . countChars) inputLines

part2 inputLines = (head . filter onlyOneCharDiff) $ map (uncurry boxIntersect) pairs
  where lineLength = length $ head inputLines
        onlyOneCharDiff s = lineLength - 1 == length s
        pairs = concat $ map (\s -> zip (repeat s) inputLines) inputLines

main :: IO ()
main = do input <- readFile "src/main/resources/Day2.txt"
          let inputLines = lines input
          putStrLn ("Part1: " ++ show (part1 inputLines))
          putStrLn ("Part2: " ++ show (part2 inputLines))

